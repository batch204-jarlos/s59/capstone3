import { useState, useContext, useEffect } from 'react';
import productData from '../data/productData';
import ProductCard from '../components/ProductCard';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';


export default function Products(){

	const [productsData, setProductsData]= useState([]);

	// console.log(productData);
	// console.log(productData[1])

	const { user } = useContext(UserContext)

	const fetchData = () => {
			fetch(`${process.env.REACT_APP_API_URL}/products/admin/all`)
			.then(res => res.json())
			.then(data => {
				// console.log(data)
				setProductsData(data)
			})

		}
		useEffect(() => {
			fetchData()

		}, [])




		const products = productsData.map(product => {
			if(product.isActive){
				return(
				<ProductCard productProp={product} key={product._id} />
				)
			}else {
				return null
			}
			
		})
		

	return(
		(user.isAdmin) ?
	  	<AdminView productsProp={productsData} fetchData={fetchData} />
	  	:
		<>
			<h1>Products</h1>
			{products}
		</>
	)
}