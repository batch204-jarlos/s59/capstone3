import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import { Redirect, Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function SpecificProduct({match}){

	const [name, setName] = useState("");
	const [productsArr, setProductsArr] = useState([]);
	const [totalAmount, setTotalAmount] = useState(0)
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [id, setId] = useState("")
	const [qty, setQty] = useState(1)

	const productId = match.params.productId;

	const token = localStorage.getItem("token")

	const { user } = useContext(UserContext);

	// console.log(productId)

	const fetchProduct = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}
	useEffect(() => {
		fetchProduct()
	}, [])

	// console.log(productId)

	const addToCart = (e) => {
		let alreadyInCart = false
		let productIndex

		// temp cart array
		let cart = []

		if(localStorage.getItem('cart')) {
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		for(let i = 0; i < cart.length; i++){
			if(cart[i].productId === id){
				alreadyInCart = true
				productIndex = i
			}
		}

		if (alreadyInCart){
			cart[productIndex].quantity += qty
			cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
		} else {
			alert("Added to Cart")
			cart.push({
				'productId' : id,
				'name' : name,
				'price': price,
				'quantity': qty,
				'subtotal': price * qty
			})
		}

		localStorage.setItem('cart', JSON.stringify(cart))
	}


	return (
		(user.id !== null) ?
		<Container className="mt-5">
			<Card>
				<Card.Body className="text-center">
					<Card.Subtitle>Name:</Card.Subtitle>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
					
					<Button className="add-to-cart" variant="primary" id="submitBtn" onClick={e => addToCart(e)}>Add to Cart</Button>
				</Card.Body>
			</Card>
			<Link to={'/products'}>Back to Products</Link>
		</Container>
		:
		<Container>
			<div>
				<p>
					Please <Link  to={`/login`}>Login</Link>
				</p>
				<p>
					Don't have an account yet? <Link to={'/register'}>Register</Link>
				</p>
			</div>
		</Container>
	)
}