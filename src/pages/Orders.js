import { useState, useEffect, useContext } from 'react';
import { Container, Card } from 'react-bootstrap';
import OrderHistoryCard from '../components/OrderHistoryCard';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';

export default function Orders(){
	// console.log(localStorage.getItem("token"))
	// const [name, setName] = useState("");
	// const [description, setDescription] = useState("");
	// const [price, setPrice] = useState(0);
	// const [purchasedOn, setPurchasedOn] = useState("")

	const [ordersData, setOrdersData] = useState([]);
	const [eachOrderData, setEachOrderData] = useState([]);
	
	const { user } = useContext(UserContext)

	console.log(user.id)

	const fetchOrderData = () => {
				fetch(`${process.env.REACT_APP_API_URL}/orders/${user.id}/all` ,{
		      	headers: {
		        	Authorization: `Bearer ${localStorage.getItem("token")}`
		      		}	
		    	})
		    	.then(res => res.json())
		    	.then(data => {
		      		console.log(data)
		      		// console.log(data[2])
		      		// console.log(data.length)
		      		setOrdersData(data)
		    })
	}

	const fetchData = () => {
		ordersData.map(order => {
			// console.log(order)
			// setTotalAmount(order.totalAmount)
			// console.log(order.products)
			return order.products.map(product => {
				fetch(`${process.env.REACT_APP_API_URL}/products/${product.productId}`)
					.then(res => res.json())
					.then(data => {
						setEachOrderData(data)
					})
				// console.log("hia")
				// console.log(product)
				// console.log(product.productId)
				
				// return <OrderCard orderProp={product} key={product.productId} />
			})
		})
	}

	
	
  // const orders = eachOrderData.map(each => {
  // 		return <OrderCard orderProp={each} key={each._id} />
  // })


  const history = ordersData.map(order => {
  			
  	  		if (order.length !== 0) {
  	  			// console.log("hi")
  			return <OrderHistoryCard orderHistoryProp={order} key={order._id} />
  		} else {
  			return null
  		}
  })



  // const orders = eachOrderData.map(each => {
  // 		console.log(each)
  // 	return <OrderHistoryCard orderHistoryProp={each} key={each._id} />
  // })

  useEffect(() => {
  			fetchOrderData();
  			fetchData();

  	  }, [])

  // const orders = eachOrder.map(order => {
// 			console.log(order)
// 			// setPurchasedOn(order.purchasedOn)
// 			// console.log(order.products)
// 			return order
// 			// order.products.map(product => {

// 				// console.log(product)
// 				// console.log(product.productId)
// 				// return <OrderCard orderProp={product} key={product.productId} />
// 			// })
			
// 		})

  		

	return (
		(user.isAdmin) ?
		   <Redirect to="/products" />
		  :
			<>
			<h1>Order History</h1>
			{history}
			</>
	)
}