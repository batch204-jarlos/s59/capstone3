import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	const { setUser } = useContext(UserContext);

	localStorage.clear();

	
	useEffect(() => {
		setUser({
			id: null,
			email: null
		})
	})

	return (
		//redirects the user to login page
		<Redirect to="/login"/>
	)
}