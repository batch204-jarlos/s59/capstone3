import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Redirect, Link } from 'react-router-dom';


export default function Login(props){

	// console.log(props)
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	const { user, setUser } = useContext(UserContext);

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details` , {	
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	function loginUser(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access)
				retrieveUserDetails(data.access)

				alert("Successfully logged in")
				props.history.push("/products");
			}else {
				alert("Login failed. Please try again")
				setEmail("")
				setPassword("")
			}
		})
	
	}


	useEffect(() => {
		if((email !== '' && password !== '')) {
			setIsActive(true)
		} else {
			setIsActive (false)
		}
	}, [email, password])

	return (
		 (user.id !== null) ?
		   <Redirect to="/" />
		   :
		   <>
		    <Form onSubmit ={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value ={email}
					onChange ={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					value ={password}
					onChange ={e => setPassword(e.target.value)}
					required
				/>
				
			</Form.Group>

			{isActive ?
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<Button className="mt-3" variant="primary"id="submitBtn" disabled>
				Submit
			</Button>


			}

			
		</Form>

		<div>
			<p>
				Don't have an account yet? <Link to={"/register"}>Register</Link>
			</p>
		</div>
		  </>
	)
}