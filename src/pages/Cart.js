import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Button,  Table } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';



export default function Cart(props) {

	// console.log(props)
	const [total, setTotal] = useState(0)
	const [cart, setCart] = useState([])
	const [ordersArr, setOrdersArr] = useState([])
	const [id, setId] = useState("")
	const [name, setName] = useState("")
	// const [description, setDescription] = useState("")
	const [qty, setQty] = useState(1)
	const [price, setPrice] = useState(0)
	const [isActive, setIsActive] = useState(false);

	const { user } = useContext(UserContext)

	const token = localStorage.getItem("token")

	useEffect(() => {
		if(localStorage.getItem('cart')) {
			setCart(JSON.parse(localStorage.getItem('cart')))
		}
	},[])


	useEffect(() => {
		let tempTotal = 0
		cart.forEach((item) => {
			tempTotal += item.subtotal
		})

		setTotal(tempTotal)
	}, [cart])


	// Adding items to Cart:
	const addToCart = () => {
		let alreadyInCart = false
		let productIndex

		// temp cart array
		let cart = []

		if(localStorage.getItem('cart')) {
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		for(let i = 0; i < cart.length; i++){
			if(cart[i].productId === id){
				alreadyInCart = true
				productIndex = i
			}
		}

		if (alreadyInCart){
			cart[productIndex].quantity += qty
			cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
		} else {
			cart.push({
				'productId' : id,
				'name' : name,
				'price': price,
				'quantity': qty,
				'subtotal': price * qty
			})
		}

		localStorage.setItem('cart', JSON.stringify(cart))
	}


	// For adjusting the quantity of an item based on input
	const qtyInput = (productId, value) => {

		let tempCart= [...cart]

		for(let i = 0; i < tempCart.length; i++) {
			if(tempCart[i].productId === productId) {

				tempCart[i].quantity = parseFloat(value)
				tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
			}
		}

		setCart(tempCart)

		localStorage.setItem('cart', JSON.stringify(tempCart))

	}

// 	const incQty = (e) => {
   //  setQty((prevQty) => prevQty + 1);
  // 	}

   // const decQty = (e) => {
   //  setQty((prevQty) => {
   //    if(prevQty - 1 < 1) return 1;
     
   //    return prevQty - 1;
   //  });
  // 	}

	// const removeItem = (productId) => {
	// 	let tempCart = [...cart]

	// 	tempCart.splice([tempCart.indexOf(productId)], 1)
		
	// 	setCart(tempCart)

	// 	localStorage.setItem('cart', JSON.stringify(tempCart))
	// }
	// console.log(cart.length)

	const removeItemFromCart = (productId) => {
		let tempCart = cart.filter(item => item.productId !== productId);

		setCart(tempCart)

		localStorage.setItem('cart', JSON.stringify(tempCart))
	}
	// console.log(cart.productId)
	// console.log(cart[1].productId)

	const checkOut = (e) => {
		let tempCart = {cart}
		let tempTotal = total

		// let tempIndex

		// for (let i = 0; i < cart.length; i++) {
		// 	if (cart[i].name !== 'undefined') {

		// 		console.log(cart[i].productId)

		// if(localStorage.getItem('cart')) {
		// 	tempCart = JSON.parse(localStorage.getItem('cart'))
		// }

		// for(let i = 0; i < tempCart.length; i++){
		// 	if(tempCart[i].productId === id) {
		// 		tempIndex = i
		// 	}
		// }
		
		fetch(`${process.env.REACT_APP_API_URL}/orders/add` , {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				
				products: [{cart}],
					// productId: ,
					// quantity: qty 
				// }]
				totalAmount: tempTotal,
				userId: user.id,
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			props.history.push("/orders")
			if (data){
				alert("Thank you for ordering!")
			} else {
				alert("Please try again")
			}
		})
	// 	} 
	// }
	}

	// console.log([cart.indexOf(productId)])
	// Getting orders
	let newCart = JSON.parse(localStorage.getItem('cart'))
	// let productId =  newCart[0].productId


	useEffect(() => {
	if((total !== 0)) {
			setIsActive(true)
	} else {
			setIsActive (false)
	}
	}, [total])



	useEffect(() => {
	const orders = 	
		 newCart.map(order => {
			// console.log(order.productId)
			return (
					<tr key={order.productId}>
					<td>{order.name}</td>
					<td>{order.price}</td>
					<td>
              		<span className="num"><input type="number" min="0" defaultValue={order.quantity} onChange={(e) => qtyInput(order.productId, e.target.value)}/></span>
              		</td>
					<td>{order.subtotal}</td>
		
					<td>
						<Button variant="primary" size="sm" onClick={() => removeItemFromCart(order.productId)}>Remove</Button>
					</td>
				</tr>

			)
		})
		setOrdersArr(orders)
	},[cart.length])


	const fetchData = () => {
			fetch(`${process.env.REACT_APP_API_URL}/products/all`)
			.then(res => res.json())
			.then(data => {
				// console.log(data)
				localStorage.setItem("products", JSON.stringify(data))
			})
		}

	useEffect(() => {
		fetchData();
	}, [])
	
	return (
		(user.isAdmin) ?
		   <Redirect to="/products" />
		  :
		 <>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the productsProp*/}
					{ordersArr}
				</tbody>
				<tfoot>
					<tr>
						<th colSpan ="4">Total</th>
						<th>{total}</th>
					</tr>
				</tfoot>
			</Table>

			{isActive ?
			<Button className="mt-3" variant="primary" type="submit" id="submitBtn" onClick={(e) => checkOut(e)}>
				Checkout
			</Button>
			:
			<Button className="mt-3" variant="primary"id="submitBtn" disabled>
				Checkout
			</Button>


			}
		</>
		

	)
} 


