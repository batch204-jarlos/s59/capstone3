import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){

	const data = {
		title: "Gut Busters",
		content: "Fitness Shop",
		destination: "/products",
		label: "Buy now!"
	}
	return (
	<>
		<Banner dataProp={data} />
		<Highlights />


	</>
	)
}