import { useState, useEffect } from 'react';
import './App.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Products from './pages/Products';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import SpecificProduct from './pages/SpecificProduct';
import Orders from './pages/Orders';
import Cart from './pages/Cart';
import Error from './pages/Error';
import { UserProvider } from './UserContext';
import { CartProvider } from './CartContext';


function App() {

  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
  })

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details` , {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // console.log(data)
      if (typeof data._id !== 'undefined') {
       setUser({
         id: data._id,
         isAdmin: data.isAdmin
       })
      } else {
         setUser({
           id: null,
           isAdmin: null
         })
      }
    })
  }, [])


 return (
  <UserProvider value={{user, setUser}}>
    <Router>
     <>
      <AppNavBar />
       <Container>
        <Switch>
          <Route exact path ="/" component={Home}/>
          <Route exact path ="/products" component={Products}/>
          <Route exact path ="/products/:productId" component={SpecificProduct}/>
          <Route exact path ="/orders" component={Orders}/>
          <Route exact path ="/cart" component={Cart}/>
          <Route exact path ="/register" component={Register}/>
          <Route exact path ="/login" component={Login}/>
          <Route exact path ="/logout" component={Logout}/>
          <Route component={Error}/>
        </Switch>
      </Container>
      </>
    </Router>
  </UserProvider>

  )
}

export default App;
