import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
    return(
        <Row className="my-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Men's Sportswear</h2>
                        </Card.Title>
                        <Card.Text>
                            Find your sport-specific clothing for your hobbies or lifestyle. Available in all sizes.
                            
                        </Card.Text>
                    </Card.Body>
                </Card>
                </Col>
                
                <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Women's Clothing</h2>
                        </Card.Title>
                        <Card.Text>
                            Find your sport-specific clothing for your hobbies or lifestyle. Available in all sizes.
                        </Card.Text>
                    </Card.Body>
                </Card>
                </Col>

                <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Equipments</h2>
                        </Card.Title>
                        <Card.Text>
                            From dumbbells to yoga mats. Bring your fit lifestyle to your home or anywhere. Shop now!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}