import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';

export default function AdminView(props){

	//destructure the productsProp and the fetchData function from Products.js
	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	//states for handling modal visibility
	const [showEdit, setShowEdit] = useState(false)
	const [showCreate, setShowCreate] = useState(false)
	const [ordersArr, setOrdersArr] = useState([])

	const token = localStorage.getItem("token")

	
	const createProduct = (f) => {
		// console.log("Hello")
		f.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/add` , {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				alert("Successfully added product")
				closeCreate()
				fetchData()
				// data.push("/products");
			} else {
				alert("Failed to add product")
			}
		})
	}
		

	const openCreate = (f) => {
		f.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/admin/all`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
		})
			setName("")
			setDescription("")
			setPrice(0)
			setShowCreate(true)

	}

	const closeCreate = () => {
		setProductId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowCreate(false)
	}




	//Functions to handle opening and closing modals
	const openEdit = (productId) => {
		// console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
		setShowEdit(true)

	}

	const closeEdit = () => {
		setProductId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
	}


	const editProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}` , {
			method: "PUT",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${token}` 
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully updated")
				// close the modal and set all states back to default values
				closeEdit()
				fetchData()
				// We call fetchData here to update the data we receive from the database
				// Calling fetchData updates our productsProp, which the useEffect below is monitoring
				// Since the productProp updates, the useEffect runs again, which re-renders our table with the updated data
			} else {
				alert("Something went wrong")
			}
		})

	}

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				alert(`Product successfully ${bool}`)

				fetchData()
			} else {
				alert("Something went wrong")
			}
		})
	}


	const getAllOrders = () => {
		
		fetch(`${process.env.REACT_APP_API_URL}/orders/all` , {
			headers: {
				Authorization: `Bearer ${token}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully updated")
				// close the modal and set all states back to default values
				closeEdit()
				fetchData()
				// We call fetchData here to update the data we receive from the database
				// Calling fetchData updates our productsProp, which the useEffect below is monitoring
				// Since the productProp updates, the useEffect runs again, which re-renders our table with the updated data
			} else {
				alert("Something went wrong")
			}
		})

	}

	useEffect(() => {
		//map through the productsProp to generate table contents
		const products = productsProp.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
							{/*Dynamically render product availability*/}
							{product.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							//dynamically render which button show depending on product availability
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		//set the ProductsArr state with the results of our mapping so that it can be used in the return statement
		setProductsArr(products)

	}, [productsProp])





	
	
	return(
		<>
			<h2>Admin Dashboard</h2>
			{/*Button for creating/adding a product*/}
			<Button className="ms-auto" variant="primary" id="submitBtn" onClick={f => openCreate(f)}>Create</Button>
			{/*Button for getting all orders*/}
			<Button className="ms-auto" variant="warning" id="submitBtn" onClick={f => openCreate(f)}>Get Order Info</Button>

			{/*Create product modal*/}
			<Modal show={showCreate} onHide={closeCreate}>
				<Form onSubmit={f => createProduct(f)}>
					<Modal.Header closeButton>
						<Modal.Title>Create Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={f => setName(f.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={f => setDescription(f.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={f => setPrice(f.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeCreate}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>



			{/*Product info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the productsProp*/}
					{productsArr}
				</tbody>
			</Table>

			{/*Edit Product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</>
	)
}
