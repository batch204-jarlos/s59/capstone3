import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { BagCheck, House, Cart } from 'react-bootstrap-icons';


export default function AppNavBar() {

	const { user } = useContext(UserContext);

	// console.log(user)

	return (
	<Navbar bg="light" expand="lg">
		<Container>
			<Link className="navbar-brand" to="/">Gut Busters</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto">
					<Link className="nav-link" to="/"><House /></Link>
					<Link className="nav-link" to="/products" exact>Products</Link>


				{(user.id !== null && user.isAdmin) ?
					<>
					<Link className="nav-link" to="/logout">Logout</Link>
					</>
					: (user.id !== null && user.isAdmin === false) ?
					<>
					<Link className="nav-link" to="/cart"><BagCheck /></Link>
					<Link className="nav-link" to="/orders"><Cart /></Link>
					<Link className="nav-link" to="/logout">Logout</Link>
					</>
					:
					<>
					<Link className="nav-link" to="/login">Login</Link>
					<Link className="nav-link" to="/register">Register</Link>
					</>
				 }


				</Nav>
			</Navbar.Collapse>
		</Container>
	</Navbar>
	)
}