import { Link } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';
import { Accordion } from 'react-bootstrap';


export default function OrderHistoryCard({orderHistoryProp}){

	const { totalAmount, purchasedOn, } = orderHistoryProp


	
	return(
		<Accordion>
			<Accordion.Item>
				<Accordion.Header> Order:  {purchasedOn} </Accordion.Header>
				<Accordion.Body>
				<div>Items : </div>
				<div>Total Amount:{totalAmount}</div>
				<div>Purchased On: {purchasedOn}</div>
				
				</Accordion.Body>
			</Accordion.Item>
		</Accordion>
	)
}
