import { Link } from 'react-router-dom';
import { Button, Card, Table } from 'react-bootstrap';


export default function CartCard({cartProp}){

	
	const {productId, name, price, quantity, subtotal} = cartProp;

	return(
		<Table striped bordered hover>
		<thead>
	      <tr key ={productId}>
	          <th>Name</th>
	          <th>Price</th>
	          <th>Quantity</th>
	          <th>Subtotal</th>
	          <th> </th>
	      </tr>
      	</thead>
      	<tbody>
        <tr>
          <td>{name}</td>
          <td>{price}</td>
          <td>{quantity}</td>
          <td>{subtotal}</td>
          <td>
          	<Button>Remove</Button>
          </td>
        </tr>
      </tbody>
		</Table>

	)
}