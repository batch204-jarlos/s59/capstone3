import { Link } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';


export default function OrderCard({orderProp}){

	const {name, price} = orderProp;

	return(
		<Card className="mb-2">
			<Card.Body>
			  <Card.Title>{name}</Card.Title>
			  {/*<Card.Subtitle>Date Purchased:</Card.Subtitle>*/}
			  {/*<Card.Text>{purchasedOn}</Card.Text>*/}
			  <Card.Subtitle>Price</Card.Subtitle>
			  <Card.Text>{price}</Card.Text>
			  {/*<Card.Subtitle>Total Amount</Card.Subtitle>*/}
			  {/*<Card.Text>Php {totalAmount}</Card.Text>*/}
			    {/*<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>*/}
			</Card.Body>
		 </Card>
	)
}