const productsData = [
		{
			id:"p001",
			name: "Shoes",
			description: "Brand new shoes",
			price: 1000,
			onOffer: true
		},
		{
			id:"p002",
			name: "Socks",
			description: "Brand new socks",
			price: 100,
			onOffer: true
		},
		{
			id:"p003",
			name: "Pants",
			description: "Brand new pants",
			price: 2000,
			onOffer: true
		}

]

export default productsData;